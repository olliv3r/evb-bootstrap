#!/bin/sh
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

WORKDIR="${WORKDIR:-/bootstrap/}"
REQUIRED_COMMANDS="
    [
    command
    docker
    echo
    eval
    exit
    getopts
    hostname
    printf
    pwd
    readlink
    set
    shift
"


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

usage()
{
    echo "Usage: ${0} -d <DHCPD_IFACE> [OPTIONS] [command [arguments...]]"
    echo "A wrapper script to instantiate the bootstrap container."
    echo "    -e  Environment variable to pass (Ex. VAR=value)"
    echo "    -h  Print usage"
    echo "    -i  Required network interface to bind to [DHCPD_IFACE]"
    echo "    -p  Bootstrapping path [BOOTSTRAP_PATH]"
    echo "    -r  Supply a custom registry image [CI_REGISTRY_IMAGE]"
    echo "    -v  Mount volumes to pass (Ex. /tmp)"
    echo
    echo "All options can also be passed in environment variables (listed between [brackets])."
}

init()
{
    src_file="$(readlink -f "${0}")"
    src_dir="${src_file%%${src_file##*/}}"

    opt_env="-e 'DHCPD_IFACE=${DHCPD_IFACE:? "Place provide a network interface."}' ${OPT_ENV:-}"
    opt_volume="-v '${BOOTSTRAP_PATH:-$(pwd)}:${WORKDIR}' ${OPT_VOLUME:-}"

    CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-$(basename "${src_dir}"):latest}"

    if ! docker pull "${CI_REGISTRY_IMAGE}" 2> /dev/null; then
        e_warn "Unable to pull docker image '${CI_REGISTRY_IMAGE}', building locally instead."
        if ! docker build --pull -t "${CI_REGISTRY_IMAGE}" "${src_dir}"; then
            e_warn "Failed to build local container, attempting existing container."
        fi
    fi

    if ! docker inspect --type image "${CI_REGISTRY_IMAGE}" 1> /dev/null; then
        e_err "Container '${CI_REGISTRY_IMAGE}' not found, cannot continue."
        exit 1
    fi
}

run_script()
{
    eval docker run \
        --network host \
        --privileged \
        --rm \
        -h "$(hostname)" \
        -i \
        -t \
        -w "${WORKDIR}" \
        "${opt_env}" \
        "${opt_volume}" \
        "${CI_REGISTRY_IMAGE}" \
        "${@}"
}

check_requirements()
{
    for cmd in ${REQUIRED_COMMANDS}; do
        if ! test_result="$(command -V "${cmd}")"; then
            test_result_fail="${test_result_fail:-}${test_result}\n"
        else
            test_result_pass="${test_result_pass:-}${test_result}\n"
        fi
    done

    if [ -n "${test_result_fail:-}" ]; then
        e_err "Self-test failed, missing dependencies."
        echo "======================================="
        echo "Passed tests:"
        # As the results contain \n, we expect these to be interpreted.
        # shellcheck disable=SC2059
        printf "${test_result_pass:-none\n}"
        echo "---------------------------------------"
        echo "Failed tests:"
        # shellcheck disable=SC2059
        printf "${test_result_fail:-none\n}"
        echo "======================================="
        exit 1
    fi
}

main()
{
    while getopts ":e:hi:p:r:v:" options; do
        case "${options}" in
        e)
            OPT_ENV="${OPT_ENV:-} -e '${OPTARG}'"
            ;;
        h)
            usage
            exit 0
            ;;
        i)
            DHCPD_IFACE="${OPTARG}"
            ;;
        p)
            BOOTSTRAP_PATH="${OPTARG}"
            ;;
        r)
            CI_REGISTRY_IMAGE="${OPTARG}"
            ;;
        v)
            OPT_VOLUME="${OPT_VOLUME:-} -v '${OPTARG}:${OPTARG}'"
            ;;
        :)
            e_err "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            e_err "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    check_requirements
    init
    run_script "${@}"
}

main "${@}"

exit 0
