#
# Copyright (C) 2019 EVBox B.V
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# For Alpine Linux, alpine:latest is actually the latest stable
# hadolint ignore=DL3007
FROM registry.hub.docker.com/library/alpine:latest

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# We want the latest stable version from the repo
# hadolint ignore=DL3013,DL3018
RUN apk add --no-cache \
        busybox-extras \
        dhcp \
        dumb-init \
        iperf3 \
        logrotate \
        tftp-hpa \
        openntpd \
    && \
    rm -rf "/var/cache/apk/"* && \
    touch "/var/lib/dhcp/dhcpd.leases"

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/dhcpd.conf" "/etc/dhcp/dhcpd.conf"
COPY "./dockerfiles/init.sh" "/init"
COPY "./dockerfiles/logrotate.conf" "/etc/logrotate.conf"
COPY "./dockerfiles/ntpd.conf" "/etc/ntpd.conf"

EXPOSE 67/udp
EXPOSE 68/udp
EXPOSE 69/udp
EXPOSE 80/tcp
EXPOSE 123/tcp
EXPOSE 123/udp
EXPOSE 5201/tcp
EXPOSE 5201/udp

CMD [ "/init" ]
