#!/usr/bin/dumb-init /bin/sh
#
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

TFTP_ROOT="/bootstrap/tftpboot/"
WWW_ROOT="/bootstrap/www/"


e_err()
{
    >&2 echo "ERROR: ${*}"
}

e_warn()
{
    echo "WARN: ${*}"
}

if ! ip address replace 192.168.163.1/24 dev "${DHCPD_IFACE:?}"; then
    e_err "Unable to configure network device '${DHCPD_IFACE}'."
    exit 1
fi
if ! ip route replace 192.168.163.0/24 dev "${DHCPD_IFACE:?}"; then
    e_err "Unable to configure route for device '${DHCPD_IFACE}'."
    exit 1
fi

if ! syslogd; then
    e_warn "Failed to start syslog, logging may not work."
fi

sleep 1

if [ ! -d "${WWW_ROOT}" ]; then
    WWW_ROOT="${TFTP_ROOT}"
fi
if ! httpd -h "${WWW_ROOT}" -p 192.168.163.1; then
    e_warn "Failed to tart httpd server."
fi

if ! in.tftpd --address 192.168.163.1 --listen --secure --verbosity=7 "${TFTP_ROOT}"; then
    e_err "Failed to start in.tftpd."
    exit 1
fi

if ! iperf3 --bind 192.168.163.1 --daemon --logfile "/var/log/iperf3.log" --server; then
    e_warn "Failed to start iperf3 server."
fi

if ! ntpd; then
    e_warn "Failed to start ntpd server."
fi

if ! dhcpd "${DHCPD_IFACE:?}"; then
    e_err "Failed to start dhcpd."
    exit 1
fi

tail -f "/var/log/messages" "/var/log/iperf3.log" &

while true; do
    sleep 3600
    if ! logrotate "/etc/logrotate.conf"; then
        e_warn "Logrotation failed."
    fi
done

exit 0
