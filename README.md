# Bootstrap
When an embedded board is completely empty, a method is required to load it up
with software to be functional. To accomplish this a few basic components are
needed. The bootstrapping server requires a dedicated Ethernet device and will
use the non-configurable address **192.168.163.1/24**.

## Using this repository
This repository is not intended to be used directly. While perfectly adequate to
do so, every project may be different. This repository should rather be
considered a rather bare template. Never the less, pull-requests always welcome.

Note, in the scripts subdirectory a script 'install.sh' is found. This script
can be used to install this repository to a local system. It currently only
works with Standard Debian. Warning, the script will modify the way networking
behaves and without a second network interface, tasks that require a internet
connection (to update etc) will not work without this.

## Wrapper
Included in this repository is script called 'docker_bootstrap.sh'. This script
can be used to simplify building the container, running docker with all its
arguments.

### Prerequisites
To ensure that the included 'docker_bootstrap.sh' script can run without
problems the script will perform a self-test at start. If there are missing
dependencies, the script will print an error and inform about what was missing.

### Usage
To simplify things more, a symlink can be put into the local path. For example
in *~/bin* or */usr/local/bin*. In the following example the script is
optionally renamed. Care is to be taken however when renaming to avoid conflicts
with other files.
```console
$ ln -s /path/to/docker_bootstrap.sh ~/bin/docker_bootstrap.sh
```

Then, the script can be called from the directory containing the binary files to
be hosted. For example using the hosts second Ethernet device *eth2*:
```console
$ cd /directory/holding/binary_output/
$ docker_bootstrap.sh -i eth2
```

Alternatively the *-p* parameter can be passed to the script containing the
directory to be served.
```console
$ docker_bootstrap.sh -i eth2 -p /directory/holding/binary_output/
```

Note, that files to be served by TFTP need to be in a sub-directory called
*tftpboot*, e.g. '/directory/holding/binary_output/tftpboot/'. Files to be
served via HTTP need to be in a sub-directory called *www*, e.g.
'/directory/holding/binary_output/www/'.

If the 'www' is not a directory, then the 'tftpboot' directory will be used
to serve the files instead.

### Options
The wrapper script can pass additional environment variables or mount points
into the container if needed. While using the option flags is straight forward,
passing more then one of these via environment variables can be tricky and
should be avoided.

Simply put however, any additional arguments will not be pre-processed and are
basically passed straight through to docker.

## Building
To create the bootstrap environment, simply build the local docker container,
or use the one from the registry.
```console
$ docker build --rm -t bootstrap:latest .
```
Take note of the used tag, 'bootstrap:latest' as this will be needed when
running the container explained below. Also note that any name can be chosen
as long as this is done consistently.

## Running
Running the container has a few notes to be very careful about. First, the
container requires access to configure the network hardware and do network
broadcast. As such, *privileged* mode is required.

Further more, the container needs access to a physical network port. On laptops
this can be realized by using the fixed Ethernet for the container and WiFi for
normal network connectivity, if so required. Alternatively a second Ethernet
device can be added to the host also.

Finally, the files to be served via the tftp server. This is best accomplished
by being in the directory that contains the files to be hosted.

For example using the hosts second Ethernet device, *eth2* would look something
like this.
```console
$ cd /directory/holding/binary_output/
$ docker run \
    --network host \
    --privileged
    --rm \
    -e DHCPD_IFACE="eth2" \
    -i \
    -t \
    -v "$(pwd):/bootstrap"
    bootstrap:latest
```

This container can also be run in the background on a dedicated server. This is
purposely left as an exercise to the reader.

## Debugging
Sometimes things do not go as expected and then it helps to be able to
investigate things.

### Running container
For a running container, `docker exec`
can be used. First, obtain the name of the container. `docker container ls`
will yield the name in the last column. Alternatively the *--name* parameter can
be supplied to docker run. Then use this name to start a program in the running
container.
```console
$ docker exec -it bootstrap-container /bin/sh
```
Will start a shell inside the container named 'bootstrap-container'. The *-it*
parameters are used to execute an *interactive* command on a *terminal* and is
only needed for a shell.

### No container
If there is no container started, simply append the command to run to the docker
run command from above. So by appending '/bin/sh' we get a shell inside said
container. No programs will be started up so that will have to be done manually,
for example by executing the commands from */init*.

### Virtual Machine
Supplied is a basic Vagrantfile that can be used with `vagrant up` to create
a Virtual machine. A few manual steps are still required however, such as adding
a second network adapter and running the actual installation (which can only be
done after adding the second adapter). The adding of the network adapter can not
be done automatically due to the fact that it is best to use the pass-through
option here which would be different for every system. Two providers are
supported, libvirt-kvm and virtualbox.

## Linting
While the CI will run the linter, it is also very convenient to run the linter
locally before pushing. To do so, run `run_linter.sh` to lint all supported
files. Note that the linter will only run if either docker or the required tools
are installed.
