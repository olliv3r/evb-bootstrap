#!/bin/sh
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

DOCKER_ARGS="--rm -v \"$(pwd):/workdir\" -w \"/workdir\""
HADOLINT_ARGS="-f tty"
HADOLINT_REPO="registry.hub.docker.com/hadolint/hadolint:latest-debian"
SHELLCHECK_ARGS="-x -C -f tty -s sh"
SHELLCHECK_REPO="registry.hub.docker.com/koalaman/shellcheck:stable"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "Run this repositories linters"
    echo "    -h   Print usage"
}

run_shellcheck()
{
    if command -v "docker" 1> /dev/null; then
        docker pull "${SHELLCHECK_REPO}" 1> /dev/null || true
        SHELLCHECK_CMD="docker run ${DOCKER_ARGS} ${SHELLCHECK_REPO}"
    elif command -v "shellcheck" 1> /dev/null; then
        SHELLCHECK_CMD="shellcheck"
    else
        echo "Neither 'shellcheck' nor 'docker' are installed to run the shellcheck linter."
        return
    fi

    find '.' \
        -type f \
        -name '*.sh' | while read -r file; do
            echo "Running shellcheck on '${file}'."
            if eval "${SHELLCHECK_CMD}" "${SHELLCHECK_ARGS}" "${file}"; then
                echo "  No problems detected."
            fi
    done
}

run_hadolint()
{
    if command -v "docker" 1> /dev/null; then
        docker pull "${HADOLINT_REPO}" 1> /dev/null || true
        HADOLINT_CMD="docker run ${DOCKER_ARGS} ${HADOLINT_REPO} hadolint"
    elif command -v "hadolint" 1> /dev/null; then
        HADOLINT_CMD="hadolint"
    else
        echo "Neither 'hadolint' nor 'docker' are installed to run the hadolint linter."
        return
    fi

    find '.' \
        -type f \
        -name 'Dockerfile' | while read -r file; do
            echo "Running hado lint on '${file}'."
            if eval "${HADOLINT_CMD}" "${HADOLINT_ARGS}" "${file}"; then
                echo "  No problems detected."
            fi
    done
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    run_hadolint
    run_shellcheck
}

main "${@}"

exit 0
